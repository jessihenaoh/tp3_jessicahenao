from math import*
import time


# exercice 1

def compte_mots(x):
    mots = x.split()
    compte = 0
    i = 0
    for i in mots:
        compte += 1
        #print(i)
    return compte

print ('le nombre de mots est: ', compte_mots(''))
print ('le nombre de mots est: ', compte_mots('il ingurgite impunément un iguane.'))
print ('le nombre de mots est: ', compte_mots('coursdeprogrammation'))
print ('le nombre de mots est: ', compte_mots('Attention aux espaces  consécutifs ou terminaux '))
print ('le nombre de mots est: ', compte_mots('Jessica Henao Henao'))


# exercice 2

def remplace_multiple(s1,s2,n): 
    result = ""
    counter = 0
    sum = n
    for element in range(0,len(s1)):
        if element == sum and counter < len(s2):
            result = result + s2[counter]
            counter = counter + 1
            sum = sum + sum
        else:
            result = result + s1[element] 
    for ele in range(counter,len(s2)):
        result = result + s2[ele]
    
    print(result)
remplace_multiple("","",2)
remplace_multiple("abacus","oiseau",2)
remplace_multiple('hirondelles','nid',3)
remplace_multiple('Henao','Jessica',1)


# exercice 3 

# Enoncé 1
def termeU (n):
    if n == 0:
        resulU = 1
    else:
        resulU = termeU(n-1) * pow (2,n) + 1
    return resulU


print ('termeU(',0,')=', termeU (0))
print ('termeU(',1,')=', termeU (1))
print ('termeU(',5,')=', termeU (5))
print ('termeU(',10,')=', termeU (10))



# Enoncé 2
def serie(n):
    s = 0
    i = 0
    for i in range(n+1):
        s = s + termeU(i)
    return (s)

print('serie(',0,')=', serie(0))
print('serie(',1,')=', serie(1))
print('serie(',5,')=', serie(5))
print('serie(',10,')=', serie(10))


#Enoncé 3
def serie_v2(n):
    u0 = 1
    u_index = u0
    s = 0
    for i in range(n+1):
        u_index = u_index * pow(2,i) + i
        s = s + u_index
    return (s)

print('seriev2(',0,')=', serie(0))
print('seriev2(',1,')=', serie(1))
print('seriev2(',5,')=', serie(5))
print('seriev2(',10,')=', serie(10))

#Enoncé 5

def temps():
    depart = time.time()
    serie(300)
    arrivee = time.time()
    print("temps passe en secondes v1: ", arrivee - depart)

temps()

def temps2():
    depart = time.time()
    serie_v2(300)
    arrivee = time.time()
    print("temps passe en secondes v2: ", arrivee - depart)

temps2()
print("la 2em et plus performante que la 1ere")


# exercise 4

def factorielle_1 (n):
    result = 1
    for i in range (n):
        result *= n-i
    return result

print ('factorielle (1): ',factorielle_1 (1))
print ('factorielle (2): ',factorielle_1 (2))
print ('factorielle (3): ',factorielle_1 (3))
print ('factorielle (4): ',factorielle_1 (4))


def factorielle_2 (n):
    if n == 0:
        result = 1
    else:
        result = n * factorielle_2(n-1)
    return result


print ('factorielle (1): ',factorielle_2 (1))
print ('factorielle (2): ',factorielle_2 (2))
print ('factorielle (3): ',factorielle_2 (3))
print ('factorielle (4): ',factorielle_2 (4))

